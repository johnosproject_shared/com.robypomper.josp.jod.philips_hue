# JOD Philips Hue - 1.0

https://philips-hue-tres.local/debug/clip.html
This JOD Distribution allow to startup and manage a JOD Agent that represent a Philips Hue gateway/hub and expose to the JOSP EcoSystem all Philips Hue's lights and switches connected to represented Hue gateway/hub.

## JOD Distribution Specs

This JOD Distribution was created and maintained as part of the John O.S. Project.

|||
|---|---|
| **Current version**    | 1.0
| **References**        | [JOD Philips Hue @ JOSP Docs](https://www.johnosproject.org/docs/references/jod_dists/jod_philips_hue/)
| **Repository**        | [com.robypomper.josp.jod.philips_hue @ Bitbucket](https://bitbucket.org/johnosproject_shared/com.robypomper.josp.jod.philips_hue/)
| **Downloads**            | [com.robypomper.josp.jod.philips_hue > Downloads @ Bitbucket](https://bitbucket.org/johnosproject_shared/com.robypomper.josp.jod.philips_hue/downloads/)

This distribution can be installed on any computer. When started, it generates new ```jod.yml``` and ```struct.jod``` files depending on the ```configs/configs.sh```
configs. Then it starts sharing Philips Hue devices to the JOSP EcoSystem like bulbs, lamps, switchers, etc...

In the ```configs/configs.sh``` file you can find and configure following properties:

| Property | Example | Description |
|----------|---------|-------------|
| **HUE_GW_NAME** | philips-hue-tres  | The gateway device's name. |
| **HUE_GW_ADDR** | $HUE_GW_NAME.local  | The gateway device's address. |
| **HUE_GW_USER** | johnny_test  | The username to be used to register this instance on the gateway. |
| **HUE_GW_DEVELOPER*** | fl3Y9u1cmKYc-DVAIa9dvxrbBVyl8kHVqYSXwqt1 | The secret corresponding to the username; if it is empty, the PRE startup script register HUE_GW_DEVELOPER on the gateway and obtain new secret (require to press the gateway's button). |
| **JOHN_OWNER*** | AAAAA-BBBBB-CCCCC  | John user's id of object's owner, if any. Otherwise keep it empty. |
| **JOHN_NAME*** | "Roberto's Philips Hue GW"  | Object's name. If it's empty a random name will be generated. |

_Properties with (*) are optionals._

## JOD Distribution Usage

### Locally JOD Instance

Each JOD Distribution comes with a set of script for local JOD Instance management.

| Command | Description |
|---------|-------------|
| Start    <br/>```$ bash start.sh```     | Start local JOD instance in background mode, logs can be retrieved via ```tail -f logs/console.log``` command |
| Stop     <br/>```$ bash stop.sh```      | Stop local JOD instance, if it's running |
| State    <br/>```$ bash state.sh```     | Print the local JOD instance state (obj's id and name, isRunning, PID...) |
| Install  <br/>```$ bash install.sh```   | Install local JOD instance as system daemon/service |
| Uninstall<br/>```$ bash uninstall.sh``` | Uninstall local JOD instance as system daemon/service |

### Remote JOD Instance

To deploy and manage a JOD instance on remote device (local computer, cloud server, object device...) please use the [John Object Remote](https://www.johnosproject.org/docs/references/tools/john_object_remote/)
tools.