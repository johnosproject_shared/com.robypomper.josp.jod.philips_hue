#!/bin/bash

################################################################################
# The John Operating System Project is the collection of software and configurations
# to generate IoT EcoSystem, like the John Operating System Platform one.
# Copyright (C) 2021 Roberto Pompermaier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

###############################################################################
# Customization:
# $JOD_DIR/scripts/pre-startup.sh
#
# This script is executed before JOD instance startup (via start.sh script).
#
# It can be customized adding more checks or operations depending on
# JOD Distribution needs.
#
#
# Artifact: JOD Dist Template
# Version:  1.0-DEV
###############################################################################

## Default init - START
JOD_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd -P)/.."
source "$JOD_DIR/scripts/libs/include.sh" "$JOD_DIR"

#DEBUG=true
[[ ! -z "$DEBUG" && "$DEBUG" == true ]] && setupLogsDebug || setupLogs
setupCallerAndScript "$0" "${BASH_SOURCE[0]}"

execScriptConfigs "$JOD_DIR/scripts/jod/jod-script-configs.sh"
execScriptConfigs "$JOD_DIR/scripts/jod/errors.sh"

# Load jod_configs.sh, exit if fails
setupJODScriptConfigs "$JOD_DIR/configs/configs.sh"
## Default init - END

JOD_CONFIG=$JOD_DIR/configs/jod.yml

logInf "PRE Startup script"

# Check supported OS
supportedOS=("Unix" "MacOS" "BSD" "Solaris" "Win32") # All OSs
failOnUnsupportedOS "${supportedOS[@]}"

# Check jq
if command -v jq &>/dev/null; then
  logInf "jq installed"
else
  echo "Missing jq, please install it"
  logFat "jq (Command-line json processor) not installed, exit"
fi

# Check gateway reachability
ping -c2 $HUE_GW_ADDR >/dev/null
if [ "$?" -eq 0 ]; then
  logInf "Philips Hue Gateway at '$HUE_GW_ADDR' address reachable"
else
  logWar "ERROR: Philips Hue Gateway at '$HUE_GW_ADDR' address NOT reachable"
  logFat "Please check that the gateway is connected on the same local network of current machine, or update your 'JOD Philips Hue' distribution configs with correct HUE_GW_NAME value" $ERR_MISSING_REQUIREMENTS
fi

# Check if there is a Philips Hue Gateway's developer already configured
if [ -z $HUE_GW_DEVELOPER ]; then
  source $JOD_DIR/scripts/hw/registerDeveloper.sh
fi
logInf "Philips Hue Gateway use developer '$HUE_GW_DEVELOPER' id"

# Generate a John Object Structure for current Philips Hue Gateway
source $JOD_DIR/scripts/hw/generateObjectStructure.sh

# if jod.yml do NOT exist
if ! test -f "$JOD_CONFIG"; then
  source $JOD_DIR/scripts/hw/generateObjectConfigs.sh
fi
