#!/bin/bash

################################################################################
# The John Operating System Project is the collection of software and configurations
# to generate IoT EcoSystem, like the John Operating System Platform one.
# Copyright (C) 2021 Roberto Pompermaier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

# Register a new developer id and save it in HUE_GW_DEVELOPER vars
#
# Fetch the Philips Hue Gateway until user press the physical button on the
# gateway. This script retry max for MAX_RETRY_COUNT times, after that prints
# an error message and exit the script execution.
#
# Artifact: JOD Philips Hue
# Version:  1.0
###############################################################################

JOD_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd -P)/../.."
source "$JOD_DIR/scripts/libs/include.sh" "$JOD_DIR"

#DEBUG=true
[[ ! -z "$DEBUG" && "$DEBUG" == true ]] && setupLogsDebug || setupLogs

setupCallerAndScript "$0" "${BASH_SOURCE[0]}"

execScriptConfigs "$JOD_DIR/scripts/jod/jod-script-configs.sh"
execScriptConfigs "$JOD_DIR/scripts/jod/errors.sh"

###############################################################################
logScriptInit

# Standard configs
MAX_RETRY_COUNT=6 # Retry times
WAIT_SECONDS=10   # Wait seconds between each retry

logInf "Registering to the Philips Hue Gateway..."

# Fetch developer username
API_DEV_ID=$(curl -sS -k --request POST --data '{"devicetype":"jod-philips-hue_0.1#'$HUE_GW_USER'"}' https://$HUE_GW_ADDR/api 2>/dev/null)
# ERROR: [{"error":{"type":101,"address":"","description":"link button not pressed"}}]
# SUCCESS: [{"success":{"username":"nyFExzgK3-7UPhsRSsywJqjgaCaZn3VIgoXc0Twq"}}]

# Retry fetch developer username
COUNT=0
while [[ ${API_DEV_ID} == *"error"* && "$COUNT" -lt "$MAX_RETRY_COUNT" ]]; do
  COUNT=$(($COUNT + 1))
  logInf "Please, press the Philips Hue Gateway button ($COUNT/$MAX_RETRY_COUNT)"
  sleep $WAIT_SECONDS
  API_DEV_ID=$(curl -sS -k --request POST --data '{"devicetype":"jod-philips-hue_0.1#'$HUE_GW_USER'"}' https://$HUE_GW_ADDR/api 2>/dev/null)
done

# Handle error on fetch developer username
if [[ ${API_DEV_ID} == *"error"* ]]; then
  logWar "Philips Hue Gateway can't register new developer"
  logFat "Please retry and press the Philips Hue Gateway button"
fi

# Extract developer username
HUE_GW_DEVELOPER=$(sed 's/.*\"username\":\"\([^][,[:space:]]*\)".*/\1/' <<<"$API_DEV_ID")

logInf "Registered to the Philips Hue Gateway successfully"
