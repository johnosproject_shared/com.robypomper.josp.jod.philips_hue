#!/bin/bash

################################################################################
# The John Operating System Project is the collection of software and configurations
# to generate IoT EcoSystem, like the John Operating System Platform one.
# Copyright (C) 2021 Roberto Pompermaier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

###############################################################################
# Usage:
# no direct usage, included from other scripts
#
# Example configs script called by JOD distribution scripts.
# This configuration can be used to customize the JOD distribution management
# like execution, installation, etc...
#
# Artifact: JOD Dist Template
# Version:  1.0.1
###############################################################################

# JOD_YML
# Absolute or $JOD_DIR relative file path for JOD config file, default $JOD_DIR/jod.yml
#export JOD_YML="jod_2.yml"

# JAVA_HOME
# Full path of JAVA's JVM (ex: $JAVA_HOME/bin/java)
#JAVA_HOME="/Library/Java/JavaVirtualMachines/jdk1.8.0_251.jdk/Contents/Home"

# ############################ #
# JOD Philips Hue hub's config #
# ############################ #
echo "WAR: Please customize 'jod_configs.sh' before execute JOD's scripts" &&
  echo "     Update the '${BASH_SOURCE}' file and delete line n°${LINENO}" &&
  exit

# ...
export HUE_GW_NAME=""

# ...
export HUE_GW_ADDR=$HUE_GW_NAME.local

# ...
export HUE_GW_USER="jod_philips_hue"

# ...
#export HUE_GW_DEVELOPER=fl3Y9u1cmKYc-DVAIa9dvxrbBVyl8kHVqYSXwqt1   # Optional

# ############################## #
# JOD Philips Hue hub's examples #
# ############################## #

#export HUE_GW_NAME="MyHue"
#export HUE_GW_ADDR="Philips-hue-MyHue.local"

#export HUE_GW_NAME="Hue Thomas"
#export HUE_GW_ADDR="Philips-hue-TreS.local"
