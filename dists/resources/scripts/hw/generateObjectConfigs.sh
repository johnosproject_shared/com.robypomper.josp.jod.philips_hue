#!/bin/bash

################################################################################
# The John Operating System Project is the collection of software and configurations
# to generate IoT EcoSystem, like the John Operating System Platform one.
# Copyright (C) 2021 Roberto Pompermaier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

# Generate a new jod.yml file in current dir for current distribution's instance.
#
# Remove current struct.jod file, if any, then replace all placeholder from
# struct
# Fetch the Philips Hue Gateway until user press the physical button on the
# gateway. This script retry max for MAX_RETRY_COUNT times, after that prints
# an error message and exit the script execution.
#
# Artifact: JOD Philips Hue
# Version:  1.0
###############################################################################

JOD_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd -P)/../.."
source "$JOD_DIR/scripts/libs/include.sh" "$JOD_DIR"

#DEBUG=true
[[ ! -z "$DEBUG" && "$DEBUG" == true ]] && setupLogsDebug || setupLogs

setupCallerAndScript "$0" "${BASH_SOURCE[0]}"

execScriptConfigs "$JOD_DIR/scripts/jod/jod-script-configs.sh"
execScriptConfigs "$JOD_DIR/scripts/jod/errors.sh"

###############################################################################
logScriptInit

# Standard configs
JOD_CONFIG_TMPL=$JOD_DIR/configs/jod_TMPL.yml # jod.yml template file
JOD_CONFIG=$JOD_DIR/configs/jod.yml           # jod.yml destination file

###############################################################################
logScriptRun

logInf "Generate John Object's configs for the Philips Hue Gateway..."
logInf "Use '$JOD_CONFIG_TMPL' as template and write results in '$JOD_CONFIG'"

# Replace placeholder with 'Configurable vars' values in jod.yml
JOHN_OWNER=${JOHN_OWNER:-00000-00000-00000}
JOHN_NAME=${JOHN_NAME:-}
sed -e "s/\${JOHN_NAME}/${JOHN_NAME}/" \
  -e "s/\${JOHN_OWNER}/${JOHN_OWNER}/" \
  $JOD_CONFIG_TMPL >$JOD_CONFIG

logInf "John Object's configs generated for the Philips Hue Gateway successfully"
