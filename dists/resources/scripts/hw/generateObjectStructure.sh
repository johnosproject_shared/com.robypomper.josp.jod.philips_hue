#!/bin/bash

################################################################################
# The John Operating System Project is the collection of software and configurations
# to generate IoT EcoSystem, like the John Operating System Platform one.
# Copyright (C) 2021 Roberto Pompermaier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

# Generate a new struc.jod file in current dir representing configured
# Philips Hue Gateway
#
# Remove current struct.jod file, if any, then replace all placeholder from
# struct
# Fetch the Philips Hue Gateway until user press the physical button on the
# gateway. This script retry max for MAX_RETRY_COUNT times, after that prints
# an error message and exit the script execution.
#
# Artifact: JOD Philips Hue
# Version:  1.0
###############################################################################

JOD_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd -P)/../.."
source "$JOD_DIR/scripts/libs/include.sh" "$JOD_DIR"
source "$JOD_DIR/scripts/jod/struct/builder.sh" "$JOD_DIR"

#DEBUG=true
[[ ! -z "$DEBUG" && "$DEBUG" == true ]] && setupLogsDebug || setupLogs

setupCallerAndScript "$0" "${BASH_SOURCE[0]}"

execScriptConfigs "$JOD_DIR/scripts/jod/jod-script-configs.sh"
execScriptConfigs "$JOD_DIR/scripts/jod/errors.sh"

###############################################################################
logScriptInit

# Standard configs
JOD_STRUCT_TMPL=$JOD_DIR/configs/struct_TMPL.jod # struct.jod template file
JOD_STRUCT_TMP=$JOD_DIR/configs/struct.jod.tmp   # struct.jod temporary file
JOD_STRUCT=$JOD_DIR/configs/struct.jod           # struct.jod destination file

logInf "Generate John Object's structure for the '$HUE_GW_NAME' Philips Hue Gateway..."

# Copy struct_tmpl.jod as new struct.jod file
cp $JOD_STRUCT_TMPL $JOD_STRUCT

# Replace placeholder with global values value
sed -e "s/\${HUE_GW_NAME}/$HUE_GW_NAME/" $JOD_STRUCT >$JOD_STRUCT_TMP && mv $JOD_STRUCT_TMP $JOD_STRUCT

# Fetch device list
API_LIGHTS=$(curl -sS -k --request GET https://$HUE_GW_ADDR/api/$HUE_GW_DEVELOPER/lights 2>/dev/null)
#echo $API_LIGHTS && exit

MODEL="Philips Hue Gateway"
BRAND="Philips"
DESCR="The Philips Hue Gateway"
DESCR_LONG="The Philips Hue Gateway that allow manage and control Hue Lights and all other compatible devices."
ROOT=$(buildComponent "Root" "$MODEL" "$BRAND" "$DESCR" "$DESCR_LONG")

# Processing each device fetched
COUNT=0
echo $API_LIGHTS | jq -c '.[]' | while read LIGHT; do
  COUNT=$((COUNT + 1))
  TYPE=$(echo "$LIGHT" | jq -r .type)
  NAME=$(echo "$LIGHT" | jq -r .name)
  MODEL=$(echo "$LIGHT" | jq -r .productname)
  logInf "- adding '$NAME' Hue's device as '$MODEL' model and '$TYPE' type"

  # Light's Components

  PULLER="http://requestUrl='https://$HUE_GW_ADDR/api/$HUE_GW_DEVELOPER/lights/$COUNT';formatType=JSON;formatPath='$.state.reachable';formatPathType=JSONPATH;requestIgnoreSSLHosts=true;"
  LIGHT_ONLINE=$(buildComponent "Online" "BooleanState" "puller" "$PULLER")

  PULLER="http://requestUrl='https://$HUE_GW_ADDR/api/$HUE_GW_DEVELOPER/lights/$COUNT';formatType=JSON;formatPath='$.state.on';formatPathType=JSONPATH;requestIgnoreSSLHosts=true;"
  EXECUTOR="http://requestUrl='http://$HUE_GW_ADDR/api/$HUE_GW_DEVELOPER/lights/$COUNT/state';requestVerb=PUT;formatType=JSON;formatPath='$.[0].success';formatPathType=JSONPATH;requestIgnoreSSLHosts=true;requestBody='{\\\"on\\\":%VAL%}'"
  LIGHT_SWITCH=$(buildComponent "Switch" "BooleanAction" "puller" "$PULLER" "$EXECUTOR")

  PULLER="http://requestUrl='https://$HUE_GW_ADDR/api/$HUE_GW_DEVELOPER/lights/$COUNT';formatType=JSON;formatPath='$.state.bri';formatPathType=JSONPATH;requestIgnoreSSLHosts=true;"
  EXECUTOR="http://requestUrl='http://$HUE_GW_ADDR/api/$HUE_GW_DEVELOPER/lights/$COUNT/state';requestVerb=PUT;formatType=JSON;formatPath='$.[0].success';formatPathType=JSONPATH;requestIgnoreSSLHosts=true;requestBody='{\\\"bri\\\":%VAL_INT%}'"
  LIGHT_BRIGHTNESS=$(buildComponent "Brightness" "RangeAction" "puller" "$PULLER" 0 254 25 "$EXECUTOR")

  PULLER="http://requestUrl='https://$HUE_GW_ADDR/api/$HUE_GW_DEVELOPER/lights/$COUNT';formatType=JSON;formatPath='$.state.sat';formatPathType=JSONPATH;requestIgnoreSSLHosts=true;"
  EXECUTOR="http://requestUrl='http://$HUE_GW_ADDR/api/$HUE_GW_DEVELOPER/lights/$COUNT/state';requestVerb=PUT;formatType=JSON;formatPath='$.[0].success';formatPathType=JSONPATH;requestIgnoreSSLHosts=true;requestBody='{\\\"sat\\\":%VAL_INT%}'"
  [ "$TYPE" == "Extended color light" ] && LIGHT_SATURATION=$(buildComponent "Saturation" "RangeAction" "puller" "$PULLER" 0 254 25 "$EXECUTOR")

  PULLER="http://requestUrl='https://$HUE_GW_ADDR/api/$HUE_GW_DEVELOPER/lights/$COUNT';formatType=JSON;formatPath='$.state.hue';formatPathType=JSONPATH;requestIgnoreSSLHosts=true;"
  EXECUTOR="http://requestUrl='http://$HUE_GW_ADDR/api/$HUE_GW_DEVELOPER/lights/$COUNT/state';requestVerb=PUT;formatType=JSON;formatPath='$.[0].success';formatPathType=JSONPATH;requestIgnoreSSLHosts=true;requestBody='{\\\"hue\\\":%VAL_INT%}'"
  [ "$TYPE" == "Extended color light" ] && LIGHT_COLOR=$(buildComponent "Color Linear" "RangeAction" "puller" "$PULLER" 0 65535 1000 "$EXECUTOR")

  # Light's Container
  LIGHT_COMP=$(buildComponent "$NAME" "Container")
  LIGHT_COMP=$(addSubComponent "$LIGHT_COMP" "$LIGHT_SWITCH")
  LIGHT_COMP=$(addSubComponent "$LIGHT_COMP" "$LIGHT_ONLINE" "$LIGHT_SWITCH" "$LIGHT_BRIGHTNESS")
  [ "$TYPE" == "Extended color light" ] && LIGHT_COMP=$(addSubComponent "$LIGHT_COMP" "$LIGHT_SATURATION" "$LIGHT_COLOR")

  ROOT=$(addSubComponent "$ROOT" "$LIGHT_COMP")
  tryPrettyFormat "$ROOT" >"$JOD_STRUCT" || logWar "Error on parsing component ($ROOT)"
done

logInf "John Object's structure generated for the Philips Hue Gateway successfully"
